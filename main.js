//Создаем функцию конструктор для Hamburger//


   function Hamburger(size, stuffing, drinking) {
    try{
     if(!size){
       throw new HamburgerException("No size given")
     }
      if(!stuffing){
       throw new HamburgerException("No stuffing added")
     }

      
      //Задаем функцию проверки и показания ошибки, если введенный материал не попадает под возможный для введения материал//
      
      var flag = false;
    for(let key in Hamburger){
      if(key.toLowerCase().indexOf("size") !== -1 && size === Hamburger[key]){
        flag = true;
      }
    }
    if(!flag){
      throw new HamburgerException("invalid size")
    };



      
       var flag = false;
    for(let key in Hamburger){
      if(key.toLowerCase().indexOf("stuffing") !== -1 && stuffing === Hamburger[key]){
        flag = true;
      }
    }
    if(!flag){
      throw new HamburgerException("invalid staffing")
    };

      
    var burgerSize = size;
    this.getSize = function () {
      return burgerSize
    };
    this.setSize = function (value) {
      return burgerSize = value
    };

    var burgerStuffing = stuffing;
    this.getStuffing = function () {
      return burgerStuffing
    };
          
        
    this.setStuffing = function (value) {
      return burgerStuffing = value
    };

        
var burgerDrinkings = stuffing;
    this.getDrinkings = function () {
      return burgerDrinkings
    };
this.setDrinkings = function (value) {
      return burgerDrinkings = value
    };

    var burgerToppings = [];
    this.getBurgerToppings = function () {
      return burgerToppings;
    }
  } 
  catch (e) {
    console.log(e.message);
  }
}


//Создаем функцию для вывода ошибок в консоль//

function HamburgerException (message) {
  this.message = message;
}


// Вводим данные для бургеров//
Hamburger.SIZE_SMALL = {
    size: 'small',
    price: 50,
    calories: 20
};
Hamburger.SIZE_LARGE = {
    size: 'large',
    price: 100,
    calories: 40
};
Hamburger.STUFFING_CHEESE = {
    stuffing: 'cheese',
    price: 10,
    calories: 20
};
Hamburger.STUFFING_SALAD = {
    stuffing: 'salad',
    price: 20,
    calories: 5
};
Hamburger.STUFFING_POTATO = {
    stuffing: 'potato',
    price: 15,
    calories: 10
};
Hamburger.TOPPING_MAYO = {
    topping: 'mayo',
    price: 20,
    calories: 5
};
Hamburger.TOPPING_SPICE = {
    topping: 'spice',
    price: 15,
    calories: 0
};
Hamburger.TOPPING_NAPKIN = {
    topping: 'napkin',
    price: 5,
    calories: 0 
};
Hamburger.DRINKING_COLA = {
    topping: 'cola',
    price: 25,
    calories: 15
};
Hamburger.DRINKING_FANTA = {
    topping: 'fanta',
    price: 15,
    calories: 30
};
Hamburger.DRINNG_WATER = {
    topping: 'water',
    price: 5,
    calories: 0 
};


//Что бы добавить добавку к гамбургеру//

Hamburger.prototype.addTopping = function (topping) {
  try {
    if (!this.getBurgerToppings().includes(topping)){ 
      return this.getBurgerToppings().push(topping)   
    }
    if (this.getBurgerToppings().includes(topping)){
      throw new HamburgerException ("dublicate topping");
    }
    else {
      throw new HamburgerException ("No toppings added");
    }
  }
  catch (e){
    console.log(e.message);
  }
};

//Что бы забрать добавку бургера, если она была добавлена//

Hamburger.prototype.removeTopping = function (topping) {
  try {
    if (this.getBurgerToppings().indexOf(topping)!== -1){
      return this.getBurgerToppings().splice(this.getBurgerToppings().indexOf(topping));
    }
    else {
      throw new HamburgerException ("topping is not correctly removed");
    }
  } catch (e){
    console.log(e.message);
  }
}



//Что бы добавить drinking к гамбургеру//

Hamburger.prototype.addDrinkings = function (drinking) {
  try {
    if (!this.getBurgerDrinkings().includes(drinking)){ 
      return this.getBurgerDrinkings().push(drinking)   
    }
    if (this.getBurgerDrinkings().includes(drinking)){
      throw new HamburgerException ("dublicate drinking");
    }
    else {
      throw new HamburgerException ("No drinkings added");
    }
  }
  catch (e){
    console.log(e.message);
  }
};

//Что бы забрать drinking бургера, если она была добавлена//

Hamburger.prototype.removeDrinkings = function (drinking) {
  try {
    if (this.getBurgerDrinkings().indexOf(drinking)!== -1){
      return this.getBurgerDrinkings().splice(this.getBurgerDrinkings().indexOf(drinking));
    }
    else {
      throw new HamburgerException ("drinking is not correctly removed");
    }
  } catch (e){
    console.log(e.message);
  }
}



            //Все дейсствия по плану ниже//


//1)Узнать размеры бургера, который был заказан. //
//2) Узнать начинку бургера. //
//3) Узнать топпинги которые были добавлены. //
//4) Узннать напитки которые были добавлены. //
//5) Узнать общее каличество калорий заказаного вами бургера. //
//6) Узнать цену заказаного вами же бургерра. //




                      //1//
Hamburger.prototype.getSize = function () {
  return this.getSize();
};


                      //2//
Hamburger.prototype.getStuffing = function () {
  return this.getStuffing();
};

                      //3//
Hamburger.prototype.getToppings = function () {
  return this.getBurgerToppings();
};

                                    //4//
Hamburger.prototype.getDrinkings = function () {
  return this.getBurgerDrinkingss();
};

                        //5//
Hamburger.prototype.calculateCalories = function () {
  return this.getBurgerToppings().reduce(function(acc, calories) {return acc + calories.calories}, 0)
  + this.getSize().calories + this.getStuffing().calories + this.getDrinkings().calories
};


                         //6//
Hamburger.prototype.calculatePrice = function () {
  return this.getBurgerToppings().reduce(function(acc, prices) {return acc + prices.price}, 0)
    + this.getSize().price + this.getStuffing().price + this.getDrinkings().price
};



                 //Проверка по данным введенным више//

var hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_POTATO); //Заказали большой бургер с картофельной начинкой//

 hamburger.addTopping(Hamburger.TOPPING_SPICE); // добавили добавку приправ//

console.log("Calories: %f", hamburger.calculateCalories()); // получили количество калорий//
console.log("Price: %f", hamburger.calculatePrice()); // получили цену бургера//

hamburger.addTopping(Hamburger.TOPPING_MAYO); // добавли майонезную приправу//
hamburger.addDrinkings(Hamburger.DRINKING_COLA);
console.log("Price with sauce: %f", hamburger.calculatePrice()); // получили цену со всеми приправами
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // Проверим большой ли бургер?
hamburger.removeTopping(Hamburger.TOPPING_SPICE); //Убрали первую добавленую приправу

console.log("Have %d toppings", hamburger.getToppings().length); // остается только одна приправ майонеза

